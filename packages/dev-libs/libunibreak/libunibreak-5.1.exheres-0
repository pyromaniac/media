# Copyright 2012-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=adah1972 release=${PN}_${PV/./_} suffix=tar.gz ]

SUMMARY="An implementation of a Unicode line breaking algorithm"
DESCRIPTION="
An implementation of the line breaking and word breaking algorithms as described in Unicode
Standard Annex 14 and Unicode Standard Annex 29.
"

LICENCES="ZLIB"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

_test_files=(
    LineBreakTest-2022-02-26.txt
    WordBreakTest-2022-02-26.txt
    GraphemeBreakTest-2022-02-26.txt
)
src_fetch_extra() {
    if expecting_tests ; then
        for file in "${_test_files[@]}" ; do
            if [[ ! -e "${FETCHEDDIR}"/${file} ]] ; then
                edo wget -O "${FETCHEDDIR}"/${file} \
                    https://www.unicode.org/Public/UNIDATA/auxiliary/${file%-????-??-??.txt}.txt
            fi
        done
    fi
}

src_unpack() {
    default

    if expecting_tests ; then
        for file in "${_test_files[@]}" ; do
            edo cp "${FETCHEDDIR}"/${file} \
                "${WORK}"/src/${file%-????-??-??.txt}.txt
        done
    fi
}

